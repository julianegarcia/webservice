<?php

namespace App\Http\Middleware;

use Closure;

class DownloadMiddleware
{
    protected $except = [
        "/dados-do-profissional",
        "/logout",
        "/store-data",
        "/"
    ];
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(\Auth::check() == true && 
            $request->user()->sigup_finished == false && 
            in_array($request->getRequestUri(), $this->except) == false){
            return redirect()->route('professional.data');
        }
        return $next($request);
    }
}
