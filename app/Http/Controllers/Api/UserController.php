<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class UserController extends Controller
{
    
    public function index()
    {
        $users = User::where('sigup_finished',true)->get();
        return $users;
    }
    
    public function find(Request $request)
    {
        if($request->has('email')){
            $user = User::where('email',$request->user_id)->get();
            return $user;
        }
        $user = User::findOrFail($request->user_id);
        return $user;
    }
    
    public function retrivePassword()
    {
        return view('auth.password.email');
    }
}
