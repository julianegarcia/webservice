<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Test;
use App\Session;


class TestsController extends Controller
{
    public function all()
    {
        $tests = Test::all();
        return $tests;
    }
    
    
    public function allSession()
    {
        $sessions = Session::take(1000)->get();
        return $sessions;
    }
    
    public function store(Request $request)
    {
        $data = $request->except(['access_token']);
        \Log::info(count($data));
        if(is_array($data) || count($data) > 1){
            foreach ($data as $session){
                $this->storeOneSession($session);
            }
        }else{
            $this->storeOneSession($session);
        }
        return [
            'created' => true
        ];
    }
    
    public function storeOneSession($session)
    {
        $session = Session::create($session);
        return $session;
    }
}
