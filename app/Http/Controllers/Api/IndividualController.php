<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\Individual as IndividualRepository;

class IndividualController extends Controller
{
    
    
    /**
     * Create API Individual
     * 
     * @param Request $request
     */
    public function create(Request $request)
    {
        \Log::info($request->all());
        return (new IndividualRepository($request))->create();
    }
    
    /**
     * Update API Individual
     * 
     * @param Request $request
     */
    public function update(Request $request)
    {
        return (new IndividualRepository($request))->update();
    }
    
    /**
     * List all individuals in the API
     * 
     * @param Request $request
     */
    public function index(Request $request)
    {
        return (new IndividualRepository($request))
                ->query()
                ->ordered()
                ->alphabet()
                ->take()
                ->skip()
                ->get();
    }
    
    /**
     * Find a individual in API
     * 
     * @param Request $request
     */
    public function find(Request $request)
    {
        return (new IndividualRepository($request))
                ->find();
    }
}
