<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Token;

class LoginController extends Controller
{

    public function login(Request $request)
    {
        $email = $request->email;
        $pass = $request->password;
        $attempt = \Auth::attempt(['email' => $email, 'password' => $pass]);
        $user = $this->getUser($email,$pass);
        return $this->loginResponse($attempt,$user);
    }
    
    public function getUser($email,$pass)
    {
        return User::where('email',$email)->first();
    }
    
    
    public function loginResponse($attempt,$user=null)
    {
        if (!$attempt) {
            return [
                'login' => false,
                'failed_login_date' => date('Y-m-d\TH:i:s.u\Z'),
            ];
        }
        
        return [
            'login' => true,
            'login_date' => date('Y-m-d\TH:i:s.u\Z'),
            'user' => [
                'id' => $user->_id,
                'title' => $user->title,
                'name'  => $user->name,
                'email' => $user->email,
                'sex'   => $user->gender,
                'createdAt' => $user->created_at->format('Y-m-d\TH:i:s.u\Z')
            ],
            'token' => Token::generateToken($user->_id)
        ];
    }

}
