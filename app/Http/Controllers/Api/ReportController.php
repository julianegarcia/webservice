<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\DownloadApp;
use App\User;
use App\Session;
use App\Http\Controllers\Controller;

class ReportController extends Controller
{

    public function index($id)
    {
        $individual = \App\Individual::find($id);

        return view('individual.reports.index', [
            'individual' => $individual
        ]);
    }
    
    public function charts(Request $request)
    {
        $individual = \App\Individual::findOrFail($request->id);
        $alphabet = $this->alphabet($request);
        $spell = $this->spell($request);
        return view('individual.reports.charts')->with([
            'individual' => $individual,
            'alfabeto' => json_encode($alphabet),
            'spell' => json_encode($spell),
        ]);
    }

    public function alphabet(Request $request)
    {
        $sessions = Session::where('idIndividual', $request->id)
                ->select('alphabetTestResults.hit', 'alphabetTestResults.letter')
                ->get();
        $right = [];
        $wrong = [];
        foreach ($sessions as $session) {
            foreach ($session->alphabetTestResults as $hit) {
                if ($hit['hit']) {
                    $right[] = ['letter' => $hit['letter'], 'hit' => $hit['hit']];
                    continue;
                }
                $wrong[] = ['letter' => $hit['letter'], 'hit' => $hit['hit']];
            }
        }
        $right = collect($right)->groupBy('letter');
        $wrong = collect($wrong)->groupBy('letter');
        $alphas = range('A', 'Z');
        foreach ($alphas as $letter) {
            $letters[] = [$letter, count($right->get($letter)), count($wrong->get($letter))];
        }

        return $letters;
    }

    public function spell(Request $request)
    {
        $sessions = Session::where('idIndividual', $request->id)
                ->select('spellingTestResults.hit', 'spellingTestResults.word')
                ->get();
        //dd($sessions);
        $words = Session::where('idIndividual', $request->id)
                        ->select('spellingTestResults.word')
                        ->distinct('spellingTestResults.word')
                        ->get()->toArray();
        //dd($words);
        $right = [];
        $wrong = [];
        foreach ($sessions as $session) {
            $hit = $session->spellingTestResults;
            if ($hit['hit']) {
                $right[] = ['letter' => $hit['word'], 'hit' => $hit['hit']];
                continue;
            }
            $wrong[] = ['letter' => $hit['word'], 'hit' => $hit['hit']];
        }
        //dd($right,$wrong);
        $right = collect($right)->groupBy('letter');
        $wrong = collect($wrong)->groupBy('letter');
        foreach ($words as $letter) {
            $letters[] = [$letter[0], count($right->get($letter[0])), count($wrong->get($letter[0]))];
        }

        return $letters;
    }
    
    
    public function sessions(Request $request)
    {
        $sessions = Session::where('idIndividual', $request->id)
                ->select('created_at')
                ->groupBy('created_at')
                ->get(); 
        dd($sessions);
        $sessionsData = [];
        foreach ($sessions as $session) {
            $sessionsData[] = [$session, 'count' => $hit['hit']];
        }
        //dd($right,$wrong);
        $right = collect($right)->groupBy('letter');
        $wrong = collect($wrong)->groupBy('letter');
        foreach ($words as $letter) {
            $letters[] = [$letter[0], count($right->get($letter[0])), count($wrong->get($letter[0]))];
        }

        return $letters;
    }
}
