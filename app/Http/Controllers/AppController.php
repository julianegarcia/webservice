<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DownloadApp;
use App\User;


class AppController extends Controller
{
    public function getDownload() 
    {
        return view('leiamais.download');
    }
    
    public function postDownload(Request $request) 
    {
        DownloadApp::create([
            'ip' => $request->ip(),
            'user_id' => $request->user()->_id,
            'version' => 'app-13-11-16.apk'
        ]);
        User::find($request->user()->_id)
                ->update(['download' => true],
                        ['upsert' =>true]);
        return response()
                ->download(public_path("apks/app-13-11-16.apk"), 'leiamais.apk');
    }
    
    public function tutorial()
    {
        return view('leiamais.tutorial');
    }
}
