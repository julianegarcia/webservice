<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Individual;

class IndividualController extends Controller
{

    public function index(Request $request)
    {
        $individuals = new Individual();
        $q = "";
        $meus_individuos = false;
        if ($request->has('q')) {
            $q = trim($request->q);
            $individuals = $individuals
                    ->orWhere('name', 'like', "%$q%")
                    ->orWhere('email', 'like', "%$q%")
                    ->orWhere('cpf', 'like', "%$q%");
        }
        if ($request->has('meus-individuos')) {
            $individuals = $individuals->where('createdBy', \Auth::user()->_id);
            $meus_individuos = true;
        }
        if ($request->has('order')) {
            $individuals = $individuals->orderBy($request->query('order'), $request->query('dir'));
        } else {
            $individuals = $individuals->orderBy('created_at', 'desc');
        }
        $individuals = $individuals->paginate(12);

        return view('individual.index', [
            'individuals' => $individuals,
            'q' => $q,
            'meus_individuos' => $meus_individuos
        ]);
    }

    public function getPersonal()
    {
        return view('individual.create.personal');
    }

    public function postPersonal(Request $request)
    {
        $this->validate($request, [
            "cpf" => "required|unique:individuals"
        ]);
        $individual = new Individual();
        $params = $request->except(['_token', 'birthday']);
        foreach ($params as $key => $value) {
            $individual->$key = $value;
        }
        $individual->birthday = implode("-", array_reverse(explode("/", $request->birthday)));
        $individual->signUpDone = false;
        $individual->createdBy = \Auth::user()->_id;
        $individual->step = 2;
        $individual->save();

        return redirect()->route('individuos.development', [
                    'individual_id' => $individual->_id
        ]);
    }

    public function getDevelopment($individual_id)
    {
        return view('individual.create.development', ['individual_id' => $individual_id]);
    }

    public function postDevelopment(Request $request)
    {
        $individual = Individual::find($request->individual_id);
        $params = $request->except('_token');
        foreach ($params as $key => $value) {
            $individual->$key = $value;
        }
        $individual->signUpDone = false;
        $individual->step = 3;
        $individual->save();

        return redirect()->route('individuos.exclusion', [
                    'individual_id' => $individual->_id
        ]);
    }

    public function getTdha($individual_id)
    {
        return view('individual.create.tdha', ['individual_id' => $individual_id]);
    }

    public function postTdha(Request $request)
    {
        $individual = Individual::find($request->individual_id);
        $individualTDHA = [];
        foreach ($request->individualTDHA as $key => $value) {
            $individualTDHA[$key] = intval($value);
        }
        $individual->individualTDHA = $individualTDHA;
        $individual->signUpDone = false;
        $individual->step = 4;
        $individual->save();
        return redirect()->route('individuos.exclusion', [
                    'individual_id' => $individual->_id
        ]);
    }

    public function getExclusion($individual_id)
    {
        return view('individual.create.exclusion', ['individual_id' => $individual_id]);
    }

    public function postExclusion(Request $request)
    {
        $individual = Individual::find($request->individual_id);
        $params = $request->except('_token');
        $individualExclusion = [];
        foreach ($request->individualExclusion as $key => $value) {
            $individualExclusion[$key] = intval($value);
        }
        $individual->individualExclusion = $individualExclusion;
        $individual->signUpDone = true;
        $individual->step = 4;
        $individual->save();

        return redirect()->route('individuos.index');
    }

    /**
     * Edit individual
     * 
     * @param type $param
     */
    public function edit($id, $name)
    {
        $individual = Individual::find($id);
        return view('individual.update.index', [
            'individual' => $individual
        ]);
    }

    /**
     * Update individual
     * 
     * @param type $id
     * @param Request $request
     */
    public function update($id, Request $request)
    {
        $user = \App\Individual::find($id);
        
        $array = $user->toArray();
        $history = new \stdClass();
        foreach ($array as $key => $value) {
            if($key == 'historic'){
                continue;
            }
            $history->$key = $value;
        }
        $user->push('historic', $history);
        
        $params = $request->except(['_token', 'individualExclusion', 'individualTDHA', 'birthday']);
        foreach ($params as $key => $value) {
            $user->$key = $value;
        }
        $exclusion = ["mentalDisability","neurologicalDisorder","hearingDeficiency","visualImpairment","nervousSystemDisease","psychosis"];
        $individualExclusion = [];
        foreach ($exclusion as $index) {
            $individualExclusion[$index] = isset($request->individualExclusion[$index])?1:0;
        }
        $individualTDHA = [];
        for ($i = 1; $i < 19; $i++) {
            $index = "question$i";
            $individualTDHA[$index] = isset($request->individualTDHA[$index])?1:0;
        }
        $user->individualExclusion = $individualExclusion;
        $user->individualTDHA = $individualTDHA;
        $user->birthday = implode("-", array_reverse(explode("/", $request->birthday)));
        $user->updatedBy = \Auth::user()->_id;
        $user->save();

        return redirect()->back()->with('success', 'Salvo individuo com sucesso!');
    }

}
