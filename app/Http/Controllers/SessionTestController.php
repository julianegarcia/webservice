<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Individual;
use App\Test;
use App\Session;

class SessionTestController
{
    public function index($id,$name,Request $request)
    {
        $individual = Individual::find($id);
        $sessions = Session::where('idIndividual',$id)->select('start','stop','idProfessional','created_at')
                ->orderBy('created_at','desc')
                ->paginate(15);
        return view('individual.tests.index',[
            'individual' => $individual,
            'sessions' => $sessions
        ]);
    }
    
    public function show(Request $request)
    {
        $tests = Session::find($request->id);
        return $tests;
    }
}
