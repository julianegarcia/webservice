<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;

class UserController extends Controller
{
    public function create()
    {
        return view('auth.professional-data');
    }
    
    public function storeProfessional(Request $request)
    {
        $this->validate($request, [
            'cpf' => 'required|unique:users'
        ]);
        $params = $request->except(['_token','birthday']);
        $professional = User::find($request->user()->_id);
        foreach ($params as $key => $value) {
            $professional->$key = $value;
        }
        $professional->birthday = implode("-",array_reverse(explode("/",$request->birthday)));
        $professional->sigup_finished = true;
        $professional->save();
        
        return redirect()
                ->route('get.download');
    }

}
