<?php

namespace App;

use Carbon\Carbon;

class Token extends \Moloquent
{
    
    protected $dates = [
        'expired_at',
    ];
    
    protected $dateFormat = 'Y-m-d\TH:i:s.u\Z';
    
    public static function generateToken($userId)
    {
        $t = new static;
        
        if($t->userHasToken($userId)){
            return $t->updateToken($userId);
        }
        return $t->createToken($userId);
    }
    
    public function userHasToken($userId)
    {
        return self::where('user_id',$userId)->exists();
    }
    
    public function createToken($userId)
    {
        $t = new self;
        $t->access_token = bin2hex(openssl_random_pseudo_bytes(23));
        $t->user_id = $userId;
        $t->expired_at = Carbon::today()->addWeek();
        $t->save();
        return [
            'access_token' => $t->access_token,
            'expired_at' => $t->expired_at->format('Y-m-d\TH:i:s.u\Z'),
            'created_at' => date('Y-m-d\TH:i:s.u\Z'),
        ];
    }
    
    public function updateToken($userId)
    {
        $t = self::where('user_id',$userId)->first();
        $t->expired_at = Carbon::today()->addWeek();
        $t->save();
        return [
            'access_token' => $t->access_token,
            'expired_at' => $t->expired_at->format('Y-m-d\TH:i:s.u\Z'),
            'created_at' => $t->created_at->format('Y-m-d\TH:i:s.u\Z'),
        ];
    }
}
