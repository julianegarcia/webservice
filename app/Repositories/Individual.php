<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Repositories;

use Illuminate\Http\Request;
use App\Individual as ModelIndividual;

/**
 * Description of Individual
 *
 * @author uriel
 */
class Individual
{

    protected $query;
    protected $request;

    public function __construct(Request $request)
    {
        $this->query = new ModelIndividual();
        $this->request = $request;
    }

    public function ordered()
    {
        if ($this->request->has('orderByDesc')) {
            $this->query
                    ->orderBy($this->request->orderByDesc, 'desc');
        } elseif ($this->request->has('orderByAsc')) {
            $this->query
                    ->orderBy($this->request->orderByAsc, 'asc');
        } else {
            $this->query
                    ->orderBy('name', 'asc')
                    ->orderBy('created_at', 'desc');
        }

        return $this;
    }

    public function query()
    {
        if ($this->request->has('query')) {
            $query = $this->request->query('query');
            $this->orWhere('name', "%$query%")
                    ->orWhere('cpf', "%$query%");
        }
        return $this;
    }

    public function alphabet()
    {
        if ($this->request->has('alphabet')) {
            $alphabet = $this->request->query('alphabet');
            $this->where('name', "$alphabet%");
        }
        return $this;
    }

    public function skip()
    {
        if ($this->request->has('skip')) {
            $skip = $this->request->query('skip');
            $this->query->skip(intval($skip));
        }
        return $this;
    }

    public function take()
    {
        if ($this->request->has('take')) {
            $take = $this->request->query('take');
            $this->query->take(intval($take));
        }
        return $this;
    }

    public function get()
    {
        $collection = $this->query->get();
        return [
            'total' => ModelIndividual::count(),
            'filtred' => $this->query->count(),
            'individuals' => $collection
        ];
    }

    public function find()
    {
        return ModelIndividual::find($this->request->individual_id);
    }

    public function create()
    {
        $validator = \Validator::make($this->request->all(), [
                    'cpf' => 'required|unique:individuals'
        ]);
        if ($validator->fails()) {
            return [
                'created' => false,
                'errors' => [
                    'code' => 054,
                    'message' => "CPF já existe no banco de dados."
                ]
            ];
        }
        try {
            $params = $this->request->except('access_token');
            $user = new ModelIndividual();
            foreach ($params as $key => $value) {
                $user->$key = $value;
            }
            $user->save();
            return [
                'created' => true,
                'idBase' => $user->_id,
                'created_at' => $user->created_at,
                'individual' => $user
            ];
        } catch (\Exception $ex) {
            return [
                'created' => false,
                'errors' => [
                    'code' => 056,
                    'message' => $ex->getMessage() . ' | File:' . $ex->getFile() . '- Line: ' . $ex->getLine()
                ]
            ];
        }
    }

    public function update()
    {
        $validator = \Validator::make($this->request->all(), [
                    '_id' => 'required'
        ]);
        if ($validator->fails()) {
            return [
                'updated' => false,
                'errors' => [
                    'code' => 021,
                    'message' => "Mongo _id do usuário não encontrado."
                ]
            ];
        }
        $params = $this->request->except('access_token');
        $user = ModelIndividual::find($this->request->get('_id'));
        //Adicionando estado anterior ao historico
        $array = $user->toArray();
        $history = new \stdClass();
        foreach ($array as $key => $value) {
            if($key == 'historic'){
                continue;
            }
            $history->$key = $value;
        }
        $user->push('historic', $history);
        
        foreach ($params as $key => $value) {
            $user->$key = $value;
        }
        $user->save();
        
        return [
                'updated' => true,
            ];
    }

}
