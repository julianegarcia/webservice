<?php


namespace App;

use Carbon\Carbon;


class Session extends \Moloquent
{
    protected $collection = "session_tests";


    protected $fillable = [
        'completeTheWordTestResults','alphabetTestResults', 'idIndividual', 
        'idProfessional','spellingTestResults','wordToImageTestResults','start','stop',
        'broke'
    ];
    
    protected $dates = [
        'start','stop',
    ];


    protected $dateFormat = 'Y-m-d\TH:i:s.u\Z';
}
