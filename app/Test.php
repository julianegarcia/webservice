<?php


namespace App;

use Carbon\Carbon;


class Test extends \Moloquent
{
    protected $collection = "tests";


    protected $fillable = [
        'word', 'options', 'incompleteWord','image','letter',
    ];
    
    protected $dateFormat = 'Y-m-d\TH:i:s.u\Z';
    
    protected $dates = [
        'stop','start'
    ];
}
