<?php

namespace App;

class Individual extends \Moloquent
{

    protected $dates = [
        'birthday'
    ];
    
    protected $dateFormat = 'Y-m-d\TH:i:s.u\Z';
    
    public function sessions()
    {
        return $this->hasMany('App\Session','idIndividual');
    }
    
}
