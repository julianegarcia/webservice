<?php

return [
    'alphabet' => 'Teste Alfabeto',
    'complete-word' => 'Teste Complete a Palavra',
    'spell-the-word' => 'Teste Soletrar a Palavra',
    'link-word-to-image' => 'Teste Assossiar Palavra e Imagem'
];

