<?php

return [
    'question1' => '1. Não consegue prestar muita atenção a detalhes ou comete erros por descuidos nos trabalhos da escola ou tarefas?',
    'question2' => '2. Tem dificuldade de manter a atenção em tarefas ou atividades?',
    'question3' => '3. Parece não estar ouvindo quando se fala diretamente com ele?',
    'question4' => '4. Não segue instruções até o fim e não termina deveres de escola, tarefas ou obrigações?',
    'question5' => '5. Tem dificuldades para organizar tarefas e atividades?',
    'question6' => '6. Evita, não gosta ou se envolve contra a vontade em tarefas que exigem esforço mental prolongado?',
    'question7' => '7. Perde coisas necessárias para atividades (p. ex: brinquedos, deveres da escola, lápis ou livros)?',
    'question8' => '8. Distrai-se com estímulos externos?',
    'question9' => '9. É esquecido em atividades do dia-a-dia?',
    'question10' => '10. Mexe com as mãos ou os pés ou se remexe na cadeira?',
    'question11' => '11. Sai do lugar na sala de aula ou em outras situações em que se espera que fique sentado?',
    'question12' => '12. Corre de um lado para o outro ou sobe nas demais coisas em situações que isto é inapropriado?',
    'question13' => '13. Tem dificuldade em brincar ou envolver-se em atividades de lazer de calma?',
    'question14' => '14. Não para ou frequentemente está a "mil por hora"?',
    'question15' => '15. Fala em excesso?',
    'question16' => '16. Responde as perguntas de forma precipitada antes delas terem sido terminadas?',
    'question17' => '17. Tem dificuldade de esperar sua vez?',
    'question18' => '18. Interrompe os outros ou se intromete (p.ex. mete-se nas conversas/jogos)?',
];

