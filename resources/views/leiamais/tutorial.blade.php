@extends('layouts.app')

<style>
    .step-list li{
        margin-bottom: 25px;
        font-size: 20px;
    }
    .panel-body p{
        margin-left: 30px; 
        font-size: 20px;
    }
    figure, figcaption{
        display: inline;
    }

</style>


@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-1 col-xs-12">
                            <img class="img-responsive" width="50" src="{{asset("img/download.png")}}" style="margin:auto">
                        </div>
                        <div class="col-md-11 col-xs-12">
                            <h4>Tutorial de instalação</h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-body">
                    <p>Siga as instruções para realizar ainstalação do aplicativo:</p>
                    <ol class="step-list">
                        <li>
                            Vá ate as <strong>Configurações</strong> do seu tablet.
                            <figure>
                                <img src="{{asset("img/configurar.png")}}" width="100" class="img-responsive tutorial-img" alt="Selecionar configurações adicionais">
                                <figcaption>
                                    Figura 1. Abrir <strong>Configurar</strong>
                                </figcaption>
                            </figure>
                        </li>
                        <li>
                            Selecione a opção <strong>Segurança</strong>.
                            <figure>
                                <img src="{{asset("img/step1.png")}}" width="500" class="img-responsive tutorial-img" alt="Selecionar configurações adicionais">
                                <figcaption>
                                    Figura 2. Selecionar opção <strong>Segurança</strong>
                                </figcaption>
                            </figure>
                        </li>
                        <li>
                            Ative a opção <strong>Fontes desconhecidas.</strong>
                            <figure>
                                <img src="{{asset("img/step2.png")}}" width="500" class="img-responsive tutorial-img" alt="Selecionar configurações adicionais">
                                <figcaption>
                                    Figura 3. Ativar opção <strong>Fontes desconhecidas</strong>
                                </figcaption>
                            </figure>
                        </li>
                        <li>
                            Faça o <strong>download</strong> do aplicativo na página <a href="{{route('get.download')}}">download clicando aqui.</a>  
                        </li>
                        <li>
                            Depois de baixar, vá até a <strong>pasta de downloads</strong> do seu tablet.
                        </li>
                        <li>
                            Clique no arquivo <strong>leiamais.apk</strong> do aplicativo leia+ e inicie a instalação.
                        </li>
                        <li>
                            Ao iniciar o aplicativo realize o <strong>login</strong> com a sua <strong>conta do website.</strong>
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection