@extends('layouts.app')

@section('content')
<link href="{{asset("css/project.css")}}" rel='stylesheet'/>

<section id="projeto" class="para-slide">
    <div class="text-box">
        <h1>
            Projeto
        </h1>
        <h2>
            Nosso projeto nasceu a partir da necessidade de 
            centralizar os dados para o diagnóstico da dislexia, 
            e pela dificuldade no acesso aos especialistas que lidam com dislexia.
        </h2>
    </div>
</section>

<section id="dislexia" class="para-slide">
    <div class="text-box">
        <h1>
            Dislexia
        </h1>
        <h2>
            Dificuldade primária do aprendizado
            abrangendo: leitura, escrita, e soletração ou uma
            combinação de duas ou três destas dificuldades.
        </h2>
    </div>
</section>

<section id="justificativa" class="para-slide">
    <div class="text-box">
        <h1>
            Justificativa
        </h1>
        <h2>
            No cenário atual de tecnologias, há falta de sistemas
            voltados especificamente para o diagnóstico de
            dislexia. Um Sistema que auxilie os profissionais que
            lidam com este transtorno torna-se desafiador, tanto
            pelo desenvolvimento de um sistema inteligente, como
            pela visão social.
        </h2>
    </div>
</section>

<section id="objetivo" class="para-slide">
    <div class="text-box">
        <h1>
            Objetivo
        </h1>
        <h2>
            Fornecer aos profissionais que diagnosticam a dislexia, uma ferramenta capaz de fornecer uma triagem do indivíduo para pré-consulta.
        </h2>
        <h3>
            Nossos objetivos específicos:
        </h3>
        <ul>
            <li>
                Disponibilizar aos profissionais que lidam com o diagnóstico da dislexia uma aplicação moderna, segura e de fácil utilização.
            </li>
            <li>
                Fornecer o histórico do indivíduo, baseado nos resultados dos testes realizados no aplicativo.
            </li>
            <li>
                Prover aos profissionais, relatórios analíticos sobre os resultados dos testes.
            </li>
            <li>
                Diminuir o tempo do diagnóstico para o indivíduo, com a concentração
                dos processos de avaliação de todos os profissionais em um só local.
            </li>
        </ul>
    </div>
</section>



<section id="como-funciona" class="para-slide">
    <div class="text-box">
        <h1>Como funciona</h1>
        <h2>
            O sistema funciona através da combinação das informações do individuo e de aplicação de testes,
            acompanhados por profissionais da área, 
            que estimulam as capacidades cognitivas dos individuos, 
            tais como visão, audição, leitura, coordenação motora, fala e pensamento lógico. 
            Afim de que, de forma simples, a possibilidade de descartar
            ou constatar a dislexia seja feita de forma simples e com a apenas um profissional.
        </h2>
    </div>
</section>


<section id="aplicativo" class="para-slide">
    <div class="text-box">
        <h1>Aplicativo</h1>

        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
                <li data-target="#myCarousel" data-slide-to="2"></li>
                <li data-target="#myCarousel" data-slide-to="3"></li>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <img src="{{asset('img/alphabet.jpeg')}}" alt="Teste de Alfabeto" width="1000">
                </div>

                <div class="item">
                    <img src="{{asset('img/spell-the-word.jpeg')}}" alt="Teste de soletração" width="1000">
                </div>

                <div class="item">
                    <img src="{{asset('img/complete-word.jpeg')}}" alt="Teste de completar palavra" width="1000">
                </div>

                <div class="item">
                    <img src="{{asset('img/link-word-to-image.jpeg')}}" alt="Teste de ligar as palavras" width="1000" >
                </div>
            </div>
            <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                <i class="fa fa-arrow-circle-left" aria-hidden="true"></i>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                <i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</section>


@endsection