@extends('layouts.app')

@section('content')
<!-- Styles -->
<style>
    html, body {
        background-color: #fff;
        color: #636b6f;
        font-family: 'Raleway', sans-serif;
        font-weight: 100;
        height: 100vh;
        margin: 0;
    }

    .full-height {
        height: 89vh;
        width: 100%;
        top: -100px;
    }

    .flex-center {
        align-items: center;
        display: flex;
        justify-content: center;
    }

    .position-ref {
        position: relative;
    }

    .top-right {
        position: absolute;
        right: 0px;
        top: 0px;
        background-color: #ec971f;
        width: 100%;
        height: 60px;
        text-align: right;
        line-height: 60px;
    }

    .content {
        text-align: center;
    }

    .links > a {
        color: #fff;
        padding: 0 25px;
        font-size: 12px;
        font-weight: 600;
        letter-spacing: .1rem;
        text-decoration: none;
        text-transform: uppercase;
        line-height: 60px;
        padding-bottom: 18px; 
    }

    .links > a:hover {
        border-bottom: 3px solid #fff; 
    }

    .m-b-md {
        margin-top: 30px;
        background-color: rgba(255,255,255,0.86);
        padding: 15% 10%;
    }
    
    #form-download a{
        margin-top: 50px;
        color: #ec971f;
    }
    body{
        background: url('/img/kids-Reading-Books-group-1.jpg') center center;
        background-repeat: no-repeat;
    }
</style>

<div class="flex-center position-ref full-height">
    <div class="content">
        <div class="title m-b-md">
            <form action="{{route('post.download')}}" method="POST" id="form-download">
                {{csrf_field()}}
                <button type="submit" class="btn btn-primary btn-lg downloadapp-button"><i class='fa fa-download' aria-hidden='true'></i> Download Leia+</button><br>
                <a href="{{route('tutorial')}}"><strong>Como instalar?</strong></a>
            </form>
        </div>
    </div>
</div>
@endsection