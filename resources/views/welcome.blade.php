@extends('layouts.app')

@section('content')

<link href="{{asset('css/welcome.css')}}" rel="stylesheet"/>
<div class="flex-center pos    ition-ref full-height">
    <div class="content">
        <div class="title m-b-md">
            <a href="{{url('/o-projeto')}}">
                <img src="{{asset("img/logoleiamais.png")}}" class="img-responsive" style="margin:auto; border-radius: 10px"/>
            </a>
            <span>Projeto Leia+</span>
        </div>
        <h2 style="color: white">
            Sistema de Triagem<br>
            Para o diagnóstico da dislexia.
        </h2>
        <a style="color: white" href="{{url('/o-projeto')}}">Conheça mais o projeto</a>
    </div>
</div>
@endsection
