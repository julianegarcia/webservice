<footer>
    <div class="container">
        <p class="map">Mapa do site:</p>
        <div class="col-md-3 col-xs-12">
            <ul class="first-level">
                <li>
                    <a href="{{url('/o-projeto')}}">O Projeto</a>
                    <ul>
                        <li><a href="{{url('/o-projeto#dislexia')}}">Dislexia</a></li>
                        <li><a href="{{url('/o-projeto#justificativa')}}">Justificativa</a></li>
                        <li><a href="{{url('/o-projeto#objetivo')}}">Objetivo</a></li>
                        <li><a href="{{url('/o-projeto#como-funciona')}}">Como funciona</a></li>
                        <li><a href="{{url('/o-projeto#aplicativo')}}">Aplicativo</a></li>
                    </ul>
                </li>
            </ul>
        </div>
        <div class="col-md-3 col-xs-12">
            <ul class="first-level">
                <li>
                    <a href="{{route('get.download')}}">Download</a>
                    <ul>
                        <li><a href="{{route('tutorial')}}">Tutorial de instalação</a></li>
                    </ul>
                </li>
            </ul>
        </div>
        <div class="col-md-3 col-xs-12">
            <ul class="first-level">
                <li>
                    <a href="{{route('individuos.index')}}">Individuos</a>
                    <ul>
                        <li>
                            <a href="{{route('individuos.index',['meus-individuos' => true])}}">Meus individuos</a>
                        </li>
                        <li>
                            Individuo
                            <ul>
                                <li>Informações do usuário</li>
                                <li>Testes</li>
                                <li>Relátorios</li>
                            </ul>
                        </li>
                        <li>
                            <a <a href="{{route('individuos.personal')}}">Cadastro de individuo</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        <div class="col-md-3 col-xs-12" style="padding: 0 25px">
            <div class="row" >
                <div class="col-md-4 col-xs-4">
                    <i class="fa fa-android" aria-hidden="true"></i>
                </div>
                <div class="col-md-8 col-xs-8">
                    <h4>
                        Disponível apenas para Android
                    </h4>
                </div>
            </div>
            <br>
            <div class="col-md-12 col-xs-12">
                <p> 
                    Desenvolvido pela equipe Leia+<br>
                    Todos os diretos reservados.
                </p>
            </div>
        </div>
    </div>
</footer>