<div class="panel panel-default">
    <div class="panel-heading">Dados Pessoais</div>
    <div class="panel-body">
        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
            <label for="name" class="control-label">Nome Completo<span class="text-danger">*</span></label>
            <div class="">
                <input id="name" type="text" class="form-control" name="name" value="{{ $individual->name }}" required autofocus>

                @if ($errors->has('name'))
                <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
                @endif
            </div>
        </div>
        <div class='row'>
            <div class="col-md-6">
                <div class="form-group{{ $errors->has('birthday') ? ' has-error' : '' }}">
                    <label for="birthday" class=" control-label">Data de Nascimento<span class="text-danger">*</span></label>

                    <div class="">
                        <input id="birthday" type="text" class="form-control" name="birthday" value="{{ $individual->birthday->format('d/m/Y') }}" required>

                        @if ($errors->has('birthday'))
                        <span class="help-block">
                            <strong>{{ $errors->first('birthday') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
            </div>
            <div class='col-md-6'>
                <div class="form-group{{ $errors->has('sex') ? ' has-error' : '' }}">
                    <label for="sex" class=" control-label">Sexo<span class="text-danger">*</span></label>

                    <div class="">
                        <select id="sex" type="text" class="form-control" name="sex" value="{{ $individual->sex }}" required>
                            <option value="">Escolha o sexo</option>
                            <option value="Masculino" <?php echo ($individual->sex === "Masculino")?"selected":""?>>Masculino</option>
                            <option value="Feminino" <?php echo ($individual->sex === "Feminino")?"selected":""?>>Feminino</option>
                        </select>

                        @if ($errors->has('sex'))
                        <span class="help-block">
                            <strong>{{ $errors->first('sex') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group{{ $errors->has('cpf') ? ' has-error' : '' }}">
            <label for="cpf" class=" control-label">CPF<span class="text-danger">*</span></label>
            <input id="cpf" type="text" class="form-control" name="cpf" value="{{ $individual->cpf }}" required>
            <span class="help-block cpf-help-block" style="display:none"><strong>CPF inválido!</strong></span>
            @if ($errors->has('cpf'))
            <span class="help-block">
                <strong>{{ $errors->first('cpf') }}</strong>
            </span>
            @endif
        </div>
        <div class="form-group{{ $errors->has('motherName') ? ' has-error' : '' }}">
            <label for="motherName" class=" control-label">Nome da Mãe ou Responsável<span class="text-danger">*</span></label>

            <div class="">
                <input id="motherName" type="text" class="form-control" value="{{ $individual->motherName }}" name="motherName" required>

                @if ($errors->has('motherName'))
                <span class="help-block">
                    <strong>{{ $errors->first('motherName') }}</strong>
                </span>
                @endif
            </div>
        </div>
        <div class="form-group{{ $errors->has('fatherName') ? ' has-error' : '' }}">
            <label for="fatherName" class=" control-label">Nome do Pai</label>

            <div class="">
                <input id="fatherName" type="text" class="form-control" value="{{ $individual->fatherName }}" name="fatherName">

                @if ($errors->has('fatherName'))
                <span class="help-block">
                    <strong>{{ $errors->first('fatherName') }}</strong>
                </span>
                @endif
            </div>
        </div>
        <div class="form-group">
            <label for="familyHistoric" class=" control-label checkbox-label">
                <input id="familyHistoric" type="checkbox" class="checkbox-inline indiviual-options" <?php echo !empty($individual->familyHistoric)?"checked":""?> value="1">
                <span>Histórico Familiar</span>
            </label>
            <textarea  class="form-control" id="familyHistoricText" <?php echo empty($individual->familyHistoric)?"style='display:none'":""?> name="familyHistoric">{{$individual->familyHistoric}}</textarea>
        </div>
        <div class="form-group">
            <label for="consanguinityHistoric" class=" control-label checkbox-label">
                <input id="consanguinityHistoric" type="checkbox" class="checkbox-inline indiviual-options" <?php echo !empty($individual->consanguinityHistoric)?"checked":""?> value="1">
                <span>Histórico de Consanguinidade</span>
            </label>
            <textarea  class="form-control" id="consanguinityHistoricText" name="consanguinityHistoric" <?php echo empty($individual->consanguinityHistoric)?"style='display:none'":""?>>{{$individual->consanguinityHistoric}}</textarea>
        </div>
        <div class="form-group">
            <label for="desiredGestation" class=" control-label checkbox-label">
                <input id="desiredGestation" type="checkbox" class="checkbox-inline indiviual-options" <?php echo !empty($individual->desiredGestation)?"checked":""?> value="1">
                <span>Gestão Desejada</span>
            </label>
            <textarea  class="form-control" id="desiredGestationText" name="desiredGestation" <?php echo empty($individual->desiredGestation)?"style='display:none'":""?>>{{$individual->desiredGestation}}</textarea>
        </div>
        <div class="form-group">
            <label for="pregnancyComplications" class=" control-label checkbox-label">
                <input id="pregnancyComplications" type="checkbox" class="checkbox-inline indiviual-options" <?php echo !empty($individual->pregnancyComplications)?"checked":""?> value="1">
                <span>Intercorrências Gestacionais</span>
            </label>
            <textarea  class="form-control" id="pregnancyComplicationsText" name="pregnancyComplications" <?php echo empty($individual->pregnancyComplications)?"style='display:none'":""?>>{{$individual->pregnancyComplications}}</textarea>
        </div>
        <div class="form-group">
            <label for="maternalHistoric" class=" control-label checkbox-label">
                <input id="maternalHistoric" type="checkbox" class="checkbox-inline indiviual-options" <?php echo !empty($individual->maternalHistoric)?"checked":""?> value="1">
                <span>Antecedentes Maternos</span>
            </label>
            <textarea  class="form-control" id="maternalHistoricText" name="maternalHistoric" <?php echo empty($individual->maternalHistoric)?"style='display:none'":""?>>{{$individual->maternalHistoric}}</textarea>
        </div>
        <div class="form-group">
            <label for="neonatalComplications" class=" control-label checkbox-label">
                <input id="neonatalComplications" type="checkbox" class="checkbox-inline indiviual-options" <?php echo !empty($individual->neonatalComplications)?"checked":""?> value="1">
                <span>Complicações no Período Neonatal</span>
            </label>
            <textarea  class="form-control" id="neonatalComplicationsText" name="neonatalComplications" <?php echo empty($individual->neonatalComplications)?"style='display:none'":""?>>{{$individual->neonatalComplications}}</textarea>
        </div>
        <div class="form-group">
            <label for="comorbidities" class=" control-label checkbox-label">
                <input id="comorbidities" type="checkbox" class="checkbox-inline indiviual-options" <?php echo !empty($individual->comorbidities)?"checked":""?>  value="1">
                <span>Presenças de Comorbidades</span>
            </label>
            <textarea  class="form-control" id="comorbiditiesText" name="comorbidities" <?php echo empty($individual->comorbidities)?"style='display:none'":""?>>{{$individual->comorbidities}}</textarea>
        </div>
        <div class="form-group{{ $errors->has('telephone') ? ' has-error' : '' }}">
            <label for="telephone" class=" control-label">Telefone</label>

            <div class="">
                <input id="telephone" type="text" class="form-control" value="{{$individual->telephone}}" name="telephone">

                @if ($errors->has('telephone'))
                <span class="help-block">
                    <strong>{{ $errors->first('telephone') }}</strong>
                </span>
                @endif
            </div>
        </div>
        <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
            <label for="address" class=" control-label">Endereço Completo</label>

            <div class="">
                <input id="address" type="text" class="form-control" value="{{$individual->address}}" name="address">

                @if ($errors->has('address'))
                <span class="help-block">
                    <strong>{{ $errors->first('address') }}</strong>
                </span>
                @endif
            </div>
        </div>
        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <label for="email" class=" control-label">E-mail do Responsável</label>

            <div class="">
                <input id="email" type="email" class="form-control" value="{{$individual->email}}" name="email">

                @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
                @endif
            </div>
        </div>
        <div class="form-group">
         <button type="submit" class="btn btn-success btn-block">Salvar</button>
        </div>
    </div>
</div>
