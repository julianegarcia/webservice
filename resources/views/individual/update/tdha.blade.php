<div class="panel panel-default">
    <div class="panel-heading">Desenvolvimento</div>
    <div class="panel-body">
        <?php $i = 0?>
        @foreach($individual->individualTDHA as $question => $value)
            <div class="form-group">
                <label for="{{"question".$i}}" class=" control-label checkbox-label">
                    <input id="{{"question".$i}}" type="checkbox" class="checkbox-inline indiviual-options" <?php echo ($value)?"checked":""?> name="individualTDHA[{{"question".$i}}]" value="1">
                    <span>@lang('questions.'.$question)</span>
                </label>
            </div>
            <?php $i++?>
        @endforeach
        <div class="form-group">
         <button type="submit" class="btn btn-success btn-block">Salvar</button>
        </div>
    </div>
</div>





