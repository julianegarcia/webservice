<div class="panel panel-default">
    <div class="panel-heading">Desenvolvimento</div>
    <div class="panel-body">
        <div class="form-group{{ $errors->has('grade') ? ' has-error' : '' }}">
            <label for="grade" class="control-label">Qual ano escolhar está em curso?<span class="text-danger">*</span></label>
            <div class="">
                <select id="grade" type="text" class="form-control" name="grade" required autofocus>
                    <option value="">Selecione</option>
                    <option value="none" <?php echo ($individual->grade=="none")?"selected":""?>>Não cursa</option>
                    <option value="1° ano" <?php echo ($individual->grade=="1° ano")?"selected":""?>>1° ano</option>
                    <option value="2° ano" <?php echo ($individual->grade=="2° ano")?"selected":""?>>2° ano</option>
                    <option value="3° ano" <?php echo ($individual->grade=="3° ano")?"selected":""?>>3° ano</option>
                    <option value="4° ano" <?php echo ($individual->grade=="4° ano")?"selected":""?>>4° ano</option>
                    <option value="5° ano" <?php echo ($individual->grade=="5° ano")?"selected":""?>>5° ano</option>
                </select>
                @if ($errors->has('grade'))
                <span class="help-block">
                    <strong>{{ $errors->first('grade') }}</strong>
                </span>
                @endif
            </div>
        </div>
        <div class="form-group">
            <label for="schoolSupport" class=" control-label checkbox-label">
                <input id="schoolSupport" type="checkbox" class="checkbox-inline indiviual-options" <?php echo empty($individual->schoolSupport)?"":"checked"?> value="1">
                <span>Recebeu reforço escolhar nos últimos 6 meses?</span>
            </label>
            <textarea  class="form-control" id="schoolSupportText" name="schoolSupport" <?php echo empty($individual->schoolSupport)?"style='display:none'":""?>>{{$individual->schoolSupport}}</textarea>
        </div>
        <div class="form-group">
            <label for="tdhaTreatment" class=" control-label checkbox-label">
                <input id="tdhaTreatment" type="checkbox" class="checkbox-inline indiviual-options" <?php echo empty($individual->tdhaTreatment)?"":"checked"?> value="1">
                <span>Recebeu tratamento regular para TDAH nos ultimos 6 meses? Ou seja, 6 meses de tratamento continuo.</span>
            </label>
            <textarea  class="form-control" id="tdhaTreatmentText" name="tdhaTreatment" <?php echo (!$individual->tdhaTreatment)?"style='display:none'":""?>>{{$individual->tdhaTreatment}}</textarea>
        </div>
        <div class="form-group">
            <label for="regularSchool" class=" control-label checkbox-label">
                <input id="regularSchool" type="checkbox" class="checkbox-inline indiviual-options" <?php echo empty($individual->regularSchool)?"":"checked"?> value="1">
                <span>Estuda em escola regular?</span>
            </label>
            <textarea  class="form-control" id="regularSchoolText" name="regularSchool" <?php echo (!$individual->regularSchool)?"style='display:none'":""?>>{{$individual->regularSchool}}</textarea>
        </div>
        <div class="form-group">
            <label for="speakProblem" class=" control-label checkbox-label">
                <input id="speakProblem" type="checkbox" class="checkbox-inline indiviual-options" <?php echo empty($individual->speakProblem)?"":"checked"?> value="1">
                <span>Apresentou atraso ou transtornos na fala ou na linguagem?</span>
            </label>
            <textarea  class="form-control" id="speakProblemText" <?php echo empty($individual->speakProblem)?"style='display:none'":""?> name="speakProblem">{{$individual->speakProblem}}</textarea>
        </div>
        <div class="form-group">
         <button type="submit" class="btn btn-success btn-block">Salvar</button>
        </div>
    </div>
</div>

