<div class="panel panel-default">
    <div class="panel-heading">Desenvolvimento</div>
    <div class="panel-body">

        <div class="form-group">
            <label for="mentalDisability" class=" control-label checkbox-label">
                <input id="mentalDisability" type="checkbox" class="checkbox-inline indiviual-options" <?php echo (isset($individual->individualExclusion["mentalDisability"])&&$individual->individualExclusion["mentalDisability"])?"checked":""?> name="individualExclusion[mentalDisability]" value="1">
                <span>Diagnótico ou evidência clínica de deficiência intelectual?</span>
            </label>
        </div>
        <div class="form-group">
            <label for="neurologicalDisorder" class=" control-label checkbox-label">
                <input id="neurologicalDisorder" type="checkbox" class="checkbox-inline indiviual-options" name="individualExclusion[neurologicalDisorder]" <?php echo (isset($individual->individualExclusion["neurologicalDisorder"])&&$individual->individualExclusion["neurologicalDisorder"])?"checked":""?> value="1">
                <span>Diagnótico ou evidência clínica de algum transtorno neurológico(acidente vascular cerebral, traumatismo cranianocom lesão)?</span>
            </label>
        </div>
        <div class="form-group">
            <label for="hearingDeficiency" class=" control-label checkbox-label">
                <input id="hearingDeficiency" type="checkbox" class="checkbox-inline indiviual-options" name="individualExclusion[hearingDeficiency]" <?php echo (isset($individual->individualExclusion["hearingDeficiency"])&&$individual->individualExclusion["hearingDeficiency"])?"checked":""?> value="1">
                <span>Presença de deficiência auditiva não corrigida?</span>
            </label>
        </div>
        <div class="form-group">
            <label for="visualImpairment" class=" control-label checkbox-label">
                <input id="visualImpairment" type="checkbox" class="checkbox-inline indiviual-options" name="individualExclusion[visualImpairment]" <?php echo (isset($individual->individualExclusion["visualImpairment"])&&$individual->individualExclusion["visualImpairment"])?"checked":""?> value="1">
                <span>Presença de deficiência visual não corrigida?</span>
            </label>
        </div>
        <div class="form-group">
            <label for="nervousSystemDisease" class=" control-label checkbox-label">
                <input id="nervousSystemDisease" type="checkbox" class="checkbox-inline indiviual-options" name="individualExclusion[nervousSystemDisease]" <?php echo (isset($individual->individualExclusion["nervousSystemDisease"])&&$individual->individualExclusion["nervousSystemDisease"])?"checked":""?> value="1">
                <span>Presença de alguma doença degenerativa do sistema nervoso?</span>
            </label>
        </div>
        <div class="form-group">
            <label for="psychosis" class=" control-label checkbox-label">
                <input id="psychosis" type="checkbox" class="checkbox-inline indiviual-options" name="individualExclusion[psychosis]" <?php echo (isset($individual->individualExclusion["psychosis"])&&$individual->individualExclusion["psychosis"])?"checked":""?> value="1">
                <span>Presença de diagnóstico de psicose ou esquizofrenia?</span>
            </label>
        </div>
        <div class="form-group">
         <button type="submit" class="btn btn-success btn-block">Salvar</button>
        </div>
    </div>
</div>





