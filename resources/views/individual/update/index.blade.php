@extends('layouts.app')

@section('content')

<style>
    .checkbox-label{
        vertical-align: sub;
    }
    input[type="checkbox"]{
        margin: 0!important;
    }
    .individual-title h3{
        display: inline;
    }
    .individual-title{
        vertical-align: middle;
    }
    .btn-group{
        margin-bottom: 20px;
    }
</style>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            @if(session('success'))
            <div class="alert alert-success alert-dismissible">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Sucesso!</strong> {{session('success')}}
            </div>
            @endif
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-1 col-xs-12">
                            <img class="img-responsive" width="50" src="{{asset("img/user-placeholder-circle.png")}}">
                        </div>
                        <div class="col-md-11 col-xs-12 individual-title">
                            Dados do individuo<br>
                            <h3 >{{$individual->name}}</h3>
                        </div>
                    </div>
                </div>
            </div>
            <div class="btn-group btn-group-justified">
                <a href="#" class="btn btn-primary disabled">Dados do Individuo</a>
                <a href="{{route('tests.index',['individual_id' => $individual->_id,'name' => str_slug($individual->name)])}}" class="btn btn-primary">Testes</a>
                <a href="{{route('reports.index',['id' => $individual->_id,'name' => str_slug($individual->name)])}}" class="btn btn-primary">Relatório</a>
            </div>
            <form action="{{route('individuos.update',['id' => $individual->_id])}}" method="POST" role="form">
                {{csrf_field()}}
                <div id="example-vertical">
                    <h3>Informações pessoais</h3>
                    <section>
                        @include('individual.update.personal')
                    </section>
                    <h3>Desenvolvimento</h3>
                    <section>
                        @include('individual.update.development')
                    </section>
                    <h3>TDHA</h3>
                    <section>
                        @include('individual.update.tdha')
                    </section>
                    <h3>Critérios de Exclusão</h3>
                    <section>
                        @include('individual.update.exclusion')
                    </section>
                </div>
            </form>
        </div>
    </div>
</div>
<script src="{{asset("plugin/jquery-cookie-master/src/jquery.cookie.js")}}"></script>
<script src="{{asset("plugin/steps/jquery.steps.min.js")}}"></script>
<script>
$("#example-vertical").steps({
    headerTag: "h3",
    bodyTag: "section",
    transitionEffect: "fade",
    autoFocus: true,
    enableAllSteps: true,
    stepsOrientation: $.fn.steps.stepsOrientation.vertical,
    saveState: true,
    enablePagination: false,
});
</script>
@endsection