@extends('layouts.app')
<style>
    .checkbox-label{
        vertical-align: sub;
    }
    input[type="checkbox"]{
        margin: 0!important;
    }
    textarea.form-control {
        display: none;
    }
</style>
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Cadastro de Individuo - Desenvolvimento</div>
                <div class="panel-body">
                    <form action="{{route('post.individuos.development')}}" method="POST" role="form">
                        {{csrf_field()}}
                        <div class="form-group{{ $errors->has('grade') ? ' has-error' : '' }}">
                            <label for="grade" class="control-label">Qual ano escolhar está em curso?<span class="text-danger">*</span></label>
                            <div class="">
                                <select id="grade" type="text" class="form-control" name="grade" value="{{ old('grade') }}" required autofocus>
                                    <option value="">Selecione</option>
                                    <option value="none">Não cursa</option>
                                    <option value="1">1° ano</option>
                                    <option value="2">2° ano</option>
                                    <option value="3">3° ano</option>
                                    <option value="4">4° ano</option>
                                    <option value="5">5° ano</option>
                                </select>
                                @if ($errors->has('grade'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('grade') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="schoolSupport" class=" control-label checkbox-label">
                                <input id="schoolSupport" type="checkbox" class="checkbox-inline indiviual-options" name="schoolSupport[check]" value="true">
                                <span>Recebeu reforço escolhar nos últimos 6 meses?</span>
                            </label>
                            <textarea  class="form-control" id="schoolSupportText" name="schoolSupport[text]"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="tdhaTreatment" class=" control-label checkbox-label">
                                <input id="tdhaTreatment" type="checkbox" class="checkbox-inline indiviual-options" name="tdhaTreatment[check]" value="true">
                                <span>Recebeu tratamento regular para TDAH nos ultimos 6 meses? Ou seja, 6 meses de tratamento continuo.</span>
                            </label>
                            <textarea  class="form-control" id="tdhaTreatmentText" name="tdhaTreatment[text]"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="regularSchool" class=" control-label checkbox-label">
                                <input id="regularSchool" type="checkbox" class="checkbox-inline indiviual-options" name="regularSchool[check]" value="true">
                                <span>Estuda em escola regular?</span>
                            </label>
                            <textarea  class="form-control" id="regularSchoolText" name="regularSchool[text]"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="speakProblem" class=" control-label checkbox-label">
                                <input id="speakProblem" type="checkbox" class="checkbox-inline indiviual-options" name="speakProblem[check]" value="true">
                                <span>Apresentou atraso ou transtornos na fala ou na linguagem?</span>
                            </label>
                            <textarea  class="form-control" id="speakProblemText" name="speakProblem[text]"></textarea>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary pull-right">Próximo Passo</button>
                        </div>
                        <input value="{{$individual_id}}" type="hidden" name="individual_id"/>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
//                $("#datepicker").datepicker();
        $('#cpf').mask('000.000.000-00', {reverse: false});
        $('#birthday').mask('00/00/0000');
        var SPMaskBehavior = function (val) {
            return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
        },
                spOptions = {
                    onKeyPress: function (val, e, field, options) {
                        field.mask(SPMaskBehavior.apply({}, arguments), options);
                    }
                };

        $('#telephone').mask(SPMaskBehavior, spOptions);

        $(".indiviual-options").click(function () {
            var id = $(this).attr("id");
            var textarea = $("#" + id + "Text");
            if (textarea.is(":visible")) {
                textarea.hide();
                return;
            }
            textarea.show();
        });
    });
</script>
@endsection

