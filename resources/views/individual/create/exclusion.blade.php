@extends('layouts.app')
<style>
    .checkbox-label{
        vertical-align: sub;
    }
    input[type="checkbox"]{
        margin: 0!important;
    }
    textarea.form-control {
        display: none;
    }
</style>
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Cadastro de Individuo - Desenvolvimento</div>
                <div class="panel-body">
                    <form action="{{route('post.individuos.exclusion')}}" method="POST" role="form">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label for="mentalDisability" class=" control-label checkbox-label">
                                <input id="mentalDisability" type="checkbox" class="checkbox-inline indiviual-options" name="individualExclusion[mentalDisability]" value="true">
                                <span>Diagnótico ou evidência clínica de deficiência intelectual?</span>
                            </label>
                        </div>
                        <div class="form-group">
                            <label for="neurologicalDisorder" class=" control-label checkbox-label">
                                <input id="neurologicalDisorder" type="checkbox" class="checkbox-inline indiviual-options" name="individualExclusion[neurologicalDisorder]" value="true">
                                <span>Diagnótico ou evidência clínica de algum transtorno neurológico(acidente vascular cerebral, traumatismo cranianocom lesão)?</span>
                            </label>
                        </div>
                        <div class="form-group">
                            <label for="hearingDeficiency" class=" control-label checkbox-label">
                                <input id="hearingDeficiency" type="checkbox" class="checkbox-inline indiviual-options" name="individualExclusion[hearingDeficiency]" value="true">
                                <span>Presença de deficiência auditiva não corrigida?</span>
                            </label>
                        </div>
                        <div class="form-group">
                            <label for="visualImpairment" class=" control-label checkbox-label">
                                <input id="visualImpairment" type="checkbox" class="checkbox-inline indiviual-options" name="individualExclusion[visualImpairment]" value="true">
                                <span>Presença de deficiência visual não corrigida?</span>
                            </label>
                        </div>
                        <div class="form-group">
                            <label for="nervousSystemDisease" class=" control-label checkbox-label">
                                <input id="nervousSystemDisease" type="checkbox" class="checkbox-inline indiviual-options" name="individualExclusion[nervousSystemDisease]" value="true">
                                <span>Presença de alguma doença degenerativa do sistema nervoso?</span>
                            </label>
                        </div>
                        <div class="form-group">
                            <label for="psychosis" class=" control-label checkbox-label">
                                <input id="psychosis" type="checkbox" class="checkbox-inline indiviual-options" name="individualExclusion[psychosis]" value="true">
                                <span>Presença de diagnóstico de psicose ou esquizofrenia?</span>
                            </label>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary pull-right">Próximo Passo</button>
                        </div>
                        <input value="{{$individual_id}}" type="hidden" name="individual_id"/>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection





