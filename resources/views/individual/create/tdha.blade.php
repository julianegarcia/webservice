@extends('layouts.app')
<style>
    .checkbox-label{
        vertical-align: sub;
    }
    input[type="checkbox"]{
        margin: 0!important;
    }
    textarea.form-control {
        display: none;
    }
</style>
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Cadastro de Individuo - Desenvolvimento</div>
                <div class="panel-body">
                    <form action="{{route('post.individuos.tdha')}}" method="POST" role="form">
                        {{csrf_field()}}
                        <?php $i = 0 ?>
                        @foreach($individual->individualTDHA as $question => $value)
                            <div class="form-group">
                                <label for="{{"question".$i}}" class=" control-label checkbox-label">
                                    <input id="{{"question".$i}}" type="checkbox" class="checkbox-inline indiviual-options" <?php echo ($value)?"checked":""?>name="individualTDHA[{{"question".$i}}]" value="1">
                                    <span>@lang('questions.'.$question)</span>
                                </label>
                            </div>
                            <?php $i++ ?>
                        @endforeach
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary pull-right">Próximo Passo</button>
                        </div>
                        <input value="{{$individual_id}}" type="hidden" name="individual_id"/>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection



