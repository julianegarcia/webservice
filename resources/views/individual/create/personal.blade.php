@extends('layouts.app')
<style>
    .checkbox-label{
        vertical-align: sub;
    }
    input[type="checkbox"]{
        margin: 0!important;
    }
    textarea.form-control {
        display: none;
    }
</style>
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Cadastro de Individuo - Dados Pessoais</div>
                <div class="panel-body">
                    <form action="{{route('post.individuos.personal')}}" method="POST" role="form" id="personal-form">
                                {{csrf_field()}}
                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label for="name" class="control-label">Nome Completo<span class="text-danger">*</span></label>
                                    <div class="">
                                        <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                        @if ($errors->has('name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class='row'>
                                    <div class="col-md-6">
                                        <div class="form-group{{ $errors->has('birthday') ? ' has-error' : '' }}">
                                            <label for="birthday" class=" control-label">Data de Nascimento<span class="text-danger">*</span></label>

                                            <div class="">
                                                <input id="birthday" type="text" class="form-control" name="birthday" value="{{ old('birthday') }}" required>

                                                @if ($errors->has('birthday'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('birthday') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class='col-md-6'>
                                        <div class="form-group{{ $errors->has('sex') ? ' has-error' : '' }}">
                                            <label for="sex" class=" control-label">Sexo<span class="text-danger">*</span></label>

                                            <div class="">
                                                <select id="sex" type="text" class="form-control" name="sex" value="{{ old('sex') }}" required>
                                                    <option value="">Escolha o sexo</option>
                                                    <option value="Masculino">Masculino</option>
                                                    <option value="Feminino">Feminino</option>
                                                </select>

                                                @if ($errors->has('sex'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('sex') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('cpf') ? ' has-error' : '' }}">
                                    <label for="cpf" class=" control-label">CPF<span class="text-danger">*</span></label>
                                        <input id="cpf" type="text" class="form-control" name="cpf" required>
                                        <span class="help-block cpf-help-block" style="display:none"><strong>CPF inválido!</strong></span>
                                        @if ($errors->has('cpf'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('cpf') }}</strong>
                                        </span>
                                        @endif
                                </div>
                                <div class="form-group{{ $errors->has('motherName') ? ' has-error' : '' }}">
                                    <label for="motherName" class=" control-label">Nome da Mãe ou Responsável<span class="text-danger">*</span></label>

                                    <div class="">
                                        <input id="motherName" type="text" class="form-control" name="motherName" required>

                                        @if ($errors->has('motherName'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('motherName') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('fatherName') ? ' has-error' : '' }}">
                                    <label for="fatherName" class=" control-label">Nome do Pai</label>

                                    <div class="">
                                        <input id="fatherName" type="text" class="form-control" name="fatherName">

                                        @if ($errors->has('fatherName'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('fatherName') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="familyHistoric" class=" control-label checkbox-label">
                                        <input id="familyHistoric" type="checkbox" class="checkbox-inline indiviual-options" name="familyHistoric[check]" value="true">
                                        <span>Histórico Familiar</span>
                                    </label>
                                    <textarea  class="form-control" id="familyHistoricText" name="familyHistoric[text]"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="consanguinityHistoric" class=" control-label checkbox-label">
                                        <input id="consanguinityHistoric" type="checkbox" class="checkbox-inline indiviual-options" name="consanguinityHistoric[check]" value="true">
                                        <span>Histórico de Consanguinidade</span>
                                    </label>
                                    <textarea  class="form-control" id="consanguinityHistoricText" name="consanguinityHistoric[text]"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="desiredGestation" class=" control-label checkbox-label">
                                        <input id="desiredGestation" type="checkbox" class="checkbox-inline indiviual-options" name="desiredGestation[check]" value="true">
                                        <span>Gestão Desejada</span>
                                    </label>
                                    <textarea  class="form-control" id="desiredGestationText" name="desiredGestation[text]"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="pregnancyComplications" class=" control-label checkbox-label">
                                        <input id="pregnancyComplications" type="checkbox" class="checkbox-inline indiviual-options" name="pregnancyComplications[check]" value="true">
                                        <span>Intercorrências Gestacionais</span>
                                    </label>
                                    <textarea  class="form-control" id="pregnancyComplicationsText" name="pregnancyComplications[text]"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="maternalHistoric" class=" control-label checkbox-label">
                                        <input id="maternalHistoric" type="checkbox" class="checkbox-inline indiviual-options" name="maternalHistoric[check]" value="true">
                                        <span>Antecedentes Maternos</span>
                                    </label>
                                    <textarea  class="form-control" id="maternalHistoricText" name="maternalHistoric[text]"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="neonatalComplications" class=" control-label checkbox-label">
                                        <input id="neonatalComplications" type="checkbox" class="checkbox-inline indiviual-options" name="neonatalComplications[check]" value="true">
                                        <span>Complicações no Período Neonatal</span>
                                    </label>
                                    <textarea  class="form-control" id="neonatalComplicationsText" name="neonatalComplications[text]"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="comorbidities" class=" control-label checkbox-label">
                                        <input id="comorbidities" type="checkbox" class="checkbox-inline indiviual-options" name="comorbidities[check]" value="true">
                                        <span>Presenças de Comorbidades</span>
                                    </label>
                                    <textarea  class="form-control" id="comorbiditiesText" name="comorbidities[text]"></textarea>
                                </div>
                                <div class="form-group{{ $errors->has('telephone') ? ' has-error' : '' }}">
                                    <label for="telephone" class=" control-label">Telefone</label>

                                    <div class="">
                                        <input id="telephone" type="text" class="form-control" name="telephone">

                                        @if ($errors->has('telephone'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('telephone') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                                    <label for="address" class=" control-label">Endereço Completo</label>

                                    <div class="">
                                        <input id="address" type="text" class="form-control" name="address">

                                        @if ($errors->has('address'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('address') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label for="email" class=" control-label">E-mail do Responsável</label>

                                    <div class="">
                                        <input id="email" type="email" class="form-control" name="email">

                                        @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary pull-right">Próximo Passo</button>
                                </div>
                            </form>
                        </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(function () {
//                $("#datepicker").datepicker();
            $('#cpf').mask('000.000.000-00', {reverse: false});
            $('#birthday').mask('00/00/0000');
            var SPMaskBehavior = function (val) {
                return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
            },
                    spOptions = {
                        onKeyPress: function (val, e, field, options) {
                            field.mask(SPMaskBehavior.apply({}, arguments), options);
                        }
                    };

            $('#telephone').mask(SPMaskBehavior, spOptions);
            
            $(".indiviual-options").click(function(){
                var id = $(this).attr("id");
                var textarea = $("#"+id+"Text");
                if(textarea.is(":visible")){
                    textarea.hide();
                    return;
                }
                textarea.show();
            });
        });
    </script>
@endsection