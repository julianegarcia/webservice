<div class="modal fade" id="testsModal" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Resultado dos testes</h4>
            </div>
            <div class="modal-body">
                <div class="panel-group">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" href="#collapse1">Teste: Alfabeto</a>
                            </h4>
                        </div>
                        <div id="collapse1" class="panel-collapse collapse">
                            <ul class="list-group">
                                <li class="list-group-item" >
                                    <div class="row">
                                        <div class="col-md-1 col-xs-1" style="text-align: center" v-for="alpha in alphabetTestResults">
                                          <h2>  @{{alpha.letter}} <h2>
                                                  <h1 v-if="alpha.hit" class="right"><i class="fa fa-check-circle-o" aria-hidden="true"></i></h1>
                                                  <h1 v-else class="wrong"><i class="fa fa-times-circle" aria-hidden="true"></i></h1>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="panel-group">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" href="#collapse2">Teste: Complete a Palavra</a>
                            </h4>
                        </div>
                        <div id="collapse2" class="panel-collapse collapse" style="text-align: center">
                            <h2 >@{{completeTheWordTestResults.word}}</h2>
                            <h1 v-if="completeTheWordTestResults.hit" class="right"><i class="fa fa-check-circle-o" aria-hidden="true"></i></h1>
                            <h1 v-else class="wrong"><i class="fa fa-times-circle" aria-hidden="true"></i></h1>
                        </div>
                    </div>
                </div>
                <div class="panel-group">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" href="#collapse3">Teste: Soletração</a>
                            </h4>
                        </div>
                        <div id="collapse3" class="panel-collapse collapse"  style="text-align: center">
                            <h2 >@{{spellingTestResults.word}}</h2>
                            <h1 v-if="spellingTestResults.hit" class="right"><i class="fa fa-check-circle-o" aria-hidden="true"></i></h1>
                            <h1 v-else class="wrong"><i class="fa fa-times-circle" aria-hidden="true"></i></h1>
                        </div>
                    </div>
                </div>
                <div class="panel-group">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" href="#collapse4">Teste: Relacionar Imagem</a>
                            </h4>
                        </div>
                        <div id="collapse4" class="panel-collapse collapse" style="text-align: center">
                            <h2>@{{wordToImageTestResults.word}}</h2>
                            <h1 v-if="wordToImageTestResults.hit" class="right"><i class="fa fa-check-circle-o" aria-hidden="true"></i></h1>
                            <h1 v-else class="wrong"><i class="fa fa-times-circle" aria-hidden="true"></i></h1>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

