@extends('layouts.app')

@section('content')
<link href="{{asset('css/tests.css')}}" rel="stylesheet"/>
<script src="https://unpkg.com/vue/dist/vue.js"></script>
<script src="https://cdn.jsdelivr.net/vue.resource/1.0.3/vue-resource.min.js"></script>


<div class="container" id="testsApp">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-1 col-xs-12">
                            <img class="img-responsive" width="50" src="{{asset("img/user-placeholder-circle.png")}}">
                        </div>
                        <div class="col-md-11 col-xs-12 individual-title">
                            Sessões de testes do individuo<br>
                            <h3 >{{$individual->name}}</h3>
                        </div>
                    </div>
                </div>
            </div>
            <div class="btn-group btn-group-justified">
                <a href="{{route('individuos.edit',['id' => $individual->_id,'name' => str_slug($individual->name)])}}" class="btn btn-primary">Dados do Individuo</a>
                <a href="#" class="btn btn-primary disabled">Testes</a>
                <a href="{{route('reports.index',['id' => $individual->_id,'name' => str_slug($individual->name)])}}" class="btn btn-primary">Relatório</a>
            </div>
            <br>
            <div class="list-group">
            @foreach($sessions as $session)
            
                <a href="#" class="list-group-item" data-toggle="modal" data-target="#testsModal" v-on:click="getTests" data-session-id="{{$session->_id}}">
                    <div class="row" data-session-id="{{$session->_id}}">
                        <div class="col-md-1" data-session-id="{{$session->_id}}">
                            <div class="teste-image" data-session-id="{{$session->_id}}">
                                <i class="fa fa-gamepad" aria-hidden="true" data-session-id="{{$session->_id}}"></i>
                            </div>
                        </div>
                        <div class="col-md-11" data-session-id="{{$session->_id}}">
                            Profissional: {{App\User::find($session->idProfessional)->name}}<br>
                            Data de aplicação: {{$session->created_at->format('d/m/Y')}} <br>
                            Duração: {{$session->stop->diffInMinutes($session->start)}} minutos | Status: {{(!$session->broke)?"Completo":"Incompleto"}}<br>
                        </div>
                    </div>
                </a>
                   
            @endforeach
            </div>
            {{$sessions->links()}}
        </div>
        @include('individual.tests.modal')
    </div>
    
</div>



<form id="form-get-session" action="{{route("tests.show",['individual_id' => $individual->_id,'name' => str_slug($individual->name)])}}"></form>

<script src="{{asset("js/modal-tests-vue.js")}}"></script>


@endsection