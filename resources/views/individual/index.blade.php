@extends('layouts.app')

@section('content')

<div class="container">
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-md-1 col-xs-12">
                    <img class="img-responsive" width="50" src="{{asset("img/user-placeholder-circle.png")}}" style="margin:auto">
                </div>
                <div class="col-md-11 col-xs-12 individual-title">
                    <h4 style="margin:0;padding:0">Individuos Cadastrados</h4><br>
                    Home/Individuos
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <a href="{{route('individuos.index')}}"><i class="fa fa-users" aria-hidden="true"></i> Todos os individuos</a> -
            <a href="{{route('individuos.index',['meus-individuos' => 'true'])}}"><i class="fa fa-users" aria-hidden="true"></i> Meus individuos</a>
        </div>
    </div>
    <form id="search-form">
        <div class="row">
            <div class="col-md-12">
                <input name="q" type="search" class="search-query form-control" value="{{$q or ""}}" placeholder="Procurar individuos por Nome, E-mail, CPF"/>
                <div class="help-block">
                    <p>Pressione Enter para realizar a pesquisa</p>
                </div>
                @if($meus_individuos)
                <input name='meus-individuos' value='true' type='hidden'/>
                @endif
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-3 col-xs-12">
                <button class="btn btn-default btn-block order-buttons"  type="submit" data-order="name" data-dir="desc" style="margin-bottom: 10px">
                    <i class="fa fa-sort-alpha-desc" aria-hidden="true"></i> Ordem alfabética decrescente
                </button>
            </div>
            <div class="col-md-3 col-xs-12">
                <button class="btn btn-default btn-block order-buttons"  type="submit" data-order="name" data-dir="asc" style="margin-bottom: 10px">
                    <i class="fa fa-sort-alpha-asc" aria-hidden="true"></i> Ordem alfabética crescente
                </button>
            </div>
            <div class="col-md-3 col-xs-12">
                <button class="btn btn-default btn-block order-buttons"  type="submit" data-order="created_at" data-dir="desc" style="margin-bottom: 10px">
                    <i class="fa fa-sort-amount-desc" aria-hidden="true"></i> Ordem de cadastro decrescente
                </button>
            </div>
            <div class="col-md-3 col-xs-12">
                <button class="btn btn-default btn-block order-buttons"  type="submit" data-order="created_at" data-dir="asc" style="margin-bottom: 10px">
                    <i class="fa fa-sort-amount-asc" aria-hidden="true"></i> Ordem de cadastro crescente
                </button>
            </div>
        </div>
    </form>
    <br>
    <div class="list-group">
        @forelse ($individuals as $individual)

        <a href="{{route('individuos.edit',['id' => $individual->_id,'name' => str_slug($individual->name)])}}" class="list-group-item">
            <div class="row">
                <div class="col-md-1 col-xs-4">
                    <img class="img-responsive" width="50" src="{{asset("img/user-placeholder-circle.png")}}">
                </div>
                <div class="col-md-11 col-xs-8">
                    {{ $individual->name }}<br>
                    {{ $individual->cpf }}
                </div>
            </div>
        </a>

        @empty
        <p>Nenhum individuo cadastrado</p>
        @endforelse
    </div>
    @if($meus_individuos)
    {{$individuals->appends(['q' => $q,'meus-individuos' => true])->links()}}
    @else
    {{$individuals->appends(['q' => $q])->links()}}
    @endif

    <a class="createIndividualButton" href="{{url('/individuos/cadastro')}}">
        <i class="fa fa-user-plus" aria-hidden="true"></i>
    </a>
</div>
<script>
    $(function () {
        $(".order-buttons").click(function (e) {
            e.preventDefault();
            var button = $(this);
            var orderInput = "<input name='order' value='" + button.data('order') + "' type='hidden'/>";
            var dirInput = "<input name='dir' value='" + button.data('dir') + "' type='hidden'/>";
            $("#search-form").append(orderInput);
            $("#search-form").append(dirInput);
            $("#search-form").submit();
        });
    });
</script>
@endsection
