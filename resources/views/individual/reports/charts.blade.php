<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="cache-control" content="max-age=0" />
        <meta http-equiv="cache-control" content="no-cache" />
        <meta http-equiv="expires" content="0" />
        <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
        <meta http-equiv="pragma" content="no-cache" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="{{asset('plugin/font-awesome/css/font-awesome.min.css')}}">
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        <script src="https://unpkg.com/vue/dist/vue.js"></script>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <script src="https://cdn.jsdelivr.net/vue.resource/1.0.3/vue-resource.min.js"></script>
        <title>Relatórios individuo</title>
    </head>
    <body>
        <span id='colchart_before' style='width: 100%; height: 500px; display: block'></span>
        <span id='colchart_after' style='width: 100%; height: 250px; display: block'></span>
        <span id='colchart_diff' style='width: 100%; height: 250px; display: block'></span>
        <span id='barchart_diff' style='width: 450px; height: 250px; display: inline-block'></span>
        
        
        
        <input type="hidden" value="{{$individual->_id}}" id="individual_id">

        <form id="form-get-session" action="{{route("tests.show",['individual_id' => $individual->_id,'name' => str_slug($individual->name)])}}"></form>
        <script>

google.charts.load('current', {packages: ['bar', 'line']});
google.charts.setOnLoadCallback(drawChartAlphabetLetters);
google.charts.setOnLoadCallback(drawChartSpell);
function drawChartAlphabetLetters() {
    var data = new google.visualization.DataTable();
    data.addColumn('string', 'Letra');
    data.addColumn('number', 'Acertos');
    data.addColumn('number', 'Erros');
    var id = $("#individual_id").val();
        data.addRows({!!$alfabeto!!});
    var colChartBefore = new google.charts.Bar(document.getElementById('colchart_before'));

    var options = {
        legend: {position: 'top'},
        height: 500,
        chart: {
            title: 'Erros e acertos de letras. Teste: Alfabeto',
            subtitle: 'Distinção dos erros e acertos do individuo no teste do alfabeto'
        }
    };
    colChartBefore.draw(data, options);
}

function drawChartSpell() {
    var data = new google.visualization.DataTable();
    data.addColumn('string', 'Palavra');
    data.addColumn('number', 'Acertos');
    data.addColumn('number', 'Erros');
    var id = $("#individual_id").val();
        data.addRows({!!$spell!!});
    var colChartBefore = new google.charts.Bar(document.getElementById('colchart_after'));

    var options = {
        legend: {position: 'top'},
        height: 500,
        chart: {
            title: 'Erros e acertos de palavras. Teste: Soletrar palavras',
            subtitle: 'Distinção dos erros e acertos do individuo no teste de soletração de palavras.'
        }
    };
    colChartBefore.draw(data, options);
}

        </script>
    </body>
</html>