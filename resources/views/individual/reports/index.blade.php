@extends('layouts.app')

@section('content')

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://unpkg.com/vue/dist/vue.js"></script>
<script src="https://cdn.jsdelivr.net/vue.resource/1.0.3/vue-resource.min.js"></script>


<div class="container" id="testsApp">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-1 col-xs-12">
                            <img class="img-responsive" width="50" src="{{asset("img/user-placeholder-circle.png")}}">
                        </div>
                        <div class="col-md-11 col-xs-12 individual-title">
                            Sessões de testes do individuo<br>
                            <h3 >{{$individual->name}}</h3>
                        </div>
                    </div>
                </div>
            </div>
            <div class="btn-group btn-group-justified">
                <a href="{{route('individuos.edit',['id' => $individual->_id,'name' => str_slug($individual->name)])}}" class="btn btn-primary">Dados do Individuo</a>
                <a href="{{route('tests.index',['individual_id' => $individual->_id,'name' => str_slug($individual->name)])}}" class="btn btn-primary">Testes</a>
                <a href="{{route('reports.index',['id' => $individual->_id,'name' => str_slug($individual->name)])}}" class="btn btn-primary disabled">Relatório</a>
            </div>
            <div class="panel panel-default">
                <div class="panel-body">
                    <span id='colchart_before' style='width: 100%; height: 500px; display: block'></span>
                    <span id='colchart_after' style='width: 100%; height: 250px; display: block'></span>
                    <span id='colchart_diff' style='width: 100%; height: 250px; display: block'></span>
                    <span id='barchart_diff' style='width: 450px; height: 250px; display: inline-block'></span>
                </div>
            </div>
        </div>

    </div>
</div>
<input type="hidden" value="{{$individual->_id}}" id="individual_id">

<form id="form-get-session" action="{{route("tests.show",['individual_id' => $individual->_id,'name' => str_slug($individual->name)])}}"></form>
<script>

google.charts.load('current', {packages: ['bar', 'line']});
google.charts.setOnLoadCallback(drawChartAlphabetLetters);
google.charts.setOnLoadCallback(drawChartSpell);
function drawChartAlphabetLetters() {
    var data = new google.visualization.DataTable();
    data.addColumn('string', 'Letra');
    data.addColumn('number', 'Acertos');
    data.addColumn('number', 'Erros');
    var id = $("#individual_id").val();
        data.addRows({!!$alfabeto!!});
    var colChartBefore = new google.charts.Bar(document.getElementById('colchart_before'));

    var options = {
        legend: {position: 'top'},
        height: 500,
        chart: {
            title: 'Erros e acertos de letras. Teste: Alfabeto',
            subtitle: 'Distinção dos erros e acertos do individuo no teste do alfabeto'
        }
    };
    colChartBefore.draw(data, options);
}

function drawChartSpell() {
    var data = new google.visualization.DataTable();
    data.addColumn('string', 'Palavra');
    data.addColumn('number', 'Acertos');
    data.addColumn('number', 'Erros');
    var id = $("#individual_id").val();
        data.addRows({!!$spell!!});
    var colChartBefore = new google.charts.Bar(document.getElementById('colchart_after'));

    var options = {
        legend: {position: 'top'},
        height: 500,
        chart: {
            title: 'Erros e acertos de palavras. Teste: Soletrar palavras',
            subtitle: 'Distinção dos erros e acertos do individuo no teste de soletração de palavras.'
        }
    };
    colChartBefore.draw(data, options);
}

        </script>

@endsection