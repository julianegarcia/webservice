@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Termine de preencher seus dados</div>
                <div class="panel-body">
                    <div class=" col-md-12 col-xs-12">
                        <form class="form-horizontal" role="form" method="POST" action="{{ route('store.data') }}">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                                        <label for="title" class="control-label">Título<span class="text-danger">*</span></label>
                                        <div class="">
                                            <select id="title" type="text" class="form-control" name="title" value="{{ old('title') }}" required autofocus>
                                                <option value="">Titulo</option>
                                                <option value="Dr.">Dr.</option>
                                                <option value="Msc.">Msc.</option>
                                                <option value="Esp.">Esp.</option>
                                            </select>
                                            @if ($errors->has('title'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('title') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-9">
                                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                        <label for="name" class="control-label">Nome Completo<span class="text-danger">*</span></label>
                                        <div class="">
                                            <input id="name" type="text" class="form-control" name="name" value="{{auth()->user()->name}}" required autofocus>

                                            @if ($errors->has('name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class='row'>
                                <div class="col-md-6">
                                    <div class="form-group{{ $errors->has('birthday') ? ' has-error' : '' }}">
                                        <label for="birthday" class=" control-label">Data de Nascimento<span class="text-danger">*</span></label>

                                        <div class="">
                                            <input id="birthday" type="text" class="form-control" name="birthday" value="{{ old('birthday') }}" required>

                                            @if ($errors->has('birthday'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('birthday') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class='col-md-6'>
                                    <div class="form-group{{ $errors->has('sex') ? ' has-error' : '' }}">
                                        <label for="sex" class=" control-label">Sexo<span class="text-danger">*</span></label>

                                        <div class="">
                                            <select id="sex" type="text" class="form-control" name="sex" value="{{ old('sex') }}" required>
                                                <option value="">Escolha o sexo</option>
                                                <option value="Masculino">Masculino</option>
                                                <option value="Feminino">Feminino</option>
                                            </select>

                                            @if ($errors->has('sex'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('sex') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('cpf') ? ' has-error' : '' }}">
                                <label for="cpf" class=" control-label">CPF<span class="text-danger">*</span></label>

                                <div class="">
                                    <input id="cpf" type="text" class="form-control" name="cpf" required>

                                    @if ($errors->has('cpf'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('cpf') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('rg') ? ' has-error' : '' }}">
                                <label for="rg" class=" control-label">RG</label>

                                <div class="">
                                    <input id="rg" type="text" class="form-control" name="rg" required>

                                    @if ($errors->has('rg'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('rg') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('field') ? ' has-error' : '' }}">
                                <label for="field" class=" control-label">Area de Atuacao<span class="text-danger">*</span></label>

                                <div class="">
                                    <select id="field" type="text" class="form-control" name="field" value="{{ old('field') }}" required>
                                        <option value="">Escolha a area de atuacao</option>
                                        <option value="Nenurologia">Neurologia</option>
                                        <option value="Pediadria">Pediadria</option>
                                        <option value="Fonoaudiologia">Fonoaudiologia</option>
                                        <option value="Pedagogia">Pedagogia</option>
                                    </select>

                                    @if ($errors->has('field'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('field') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('function') ? ' has-error' : '' }}">
                                <label for="function" class=" control-label">Funcao<span class="text-danger">*</span></label>

                                <div class="">
                                    <input id="function" type="text" class="form-control" name="function" required>

                                    @if ($errors->has('function'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('function') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-block pull-right">
                                        Finalizar Cadastro e Realizar Download do App
                                    </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


