<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::any('user/login','LoginController@login');
Route::post('user/','UserController@find');
Route::post('user/email/password','UserController@retrivePassword');
Route::post('users/get-all','UserController@index');

Route::group(['prefix' => 'individuals'], function(){
    Route::post('/', 'IndividualController@index');
    Route::post('/find', 'IndividualController@find');
    Route::post('/create', 'IndividualController@create');
    Route::post('/update', 'IndividualController@update');
    Route::any('/charts', 'ReportController@charts');
});

Route::group(['prefix' => 'testes'], function(){
    Route::post('/get-all', 'TestsController@all');
    Route::post('/sessions/all', 'TestsController@allSession');
    Route::post('/store', 'TestsController@store');
});


