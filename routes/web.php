<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | This file is where you may define all of the routes that are handled
  | by your application. Just tell Laravel the URIs it should respond
  | to using a Closure or controller method. Build something great!
  |
 */


Route::group(['prefix' => 'professional'], function() {
    Route::post('create', 'UserController@create');
});

Route::get('/dados-do-profissional', ['as' => 'professional.data', 'uses' => 'UserController@create']);

Route::post('/store-data', ['as' => 'store.data', 'uses' => 'UserController@storeProfessional']);

Route::get('app/download', ['middleware' => 'download', 'as' => 'get.download', 'uses' => 'AppController@getDownload']);
Route::get('app/tutorial', ['middleware' => 'download', 'as' => 'tutorial', 'uses' => 'AppController@tutorial']);
Route::post('app/download', ['middleware' => 'download', 'as' => 'post.download', 'uses' => 'AppController@postDownload']);

Route::group(['prefix' => 'individuos'], function() {
    Route::get('/', ['as' => 'individuos.index', 'uses' => 'IndividualController@index']);
    Route::get('/cadastro', ['as' => 'individuos.personal', 'uses' => 'IndividualController@getPersonal']);
    Route::post('/cadastro', ['as' => 'post.individuos.personal', 'uses' => 'IndividualController@postPersonal']);
    Route::get('{individual_id}/cadastro/desenvolvimento', ['as' => 'individuos.development', 'uses' => 'IndividualController@getDevelopment']);
    Route::post('/cadastro/desenvolvimento', ['as' => 'post.individuos.development', 'uses' => 'IndividualController@postDevelopment']);
    Route::get('{individual_id}/cadastro/tdha', ['as' => 'individuos.tdha', 'uses' => 'IndividualController@getTdha']);
    Route::post('/cadastro/tdha', ['as' => 'post.individuos.tdha', 'uses' => 'IndividualController@postTdha']);
    Route::get('{individual_id}/cadastro/exclusoes', ['as' => 'individuos.exclusion', 'uses' => 'IndividualController@getExclusion']);
    Route::post('/cadastro/exclusoes', ['as' => 'post.individuos.exclusion', 'uses' => 'IndividualController@postExclusion']);
    Route::get('/editar/{id}/{name}', ['as' => 'individuos.edit', 'uses' => 'IndividualController@edit']);
    Route::post('/update/{id}', ['as' => 'individuos.update', 'uses' => 'IndividualController@update']);
});

Route::group(['prefix' => 'individuos/{individual_id}/{name}/sessions'], function() {
    Route::get('/', ['as' => 'tests.index', 'uses' => 'SessionTestController@index']);
    Route::post('/show', ['as' => 'tests.show', 'uses' => 'SessionTestController@show']);
    Route::get('/relatorios-do-individuo', ['as' => 'reports.index', 'uses' => 'ReportController@index']);
});

Route::get('/report/alphabet', ['as' => 'reports.alphabet', 'uses' => 'ReportController@alphabet']);
Route::get('/report/spell', ['as' => 'reports.spell', 'uses' => 'ReportController@spell']);
Route::get('/report/sessions', ['as' => 'reports.sessions', 'uses' => 'ReportController@sessions']);
Route::get('/report/', ['as' => 'reports.', 'uses' => 'ReportController@index']);


