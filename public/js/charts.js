ALPHABET=[];
SPELL=[];
$(function(){
   var id = $("#individual_id").val();
   $.get('/report/alphabet?id=' + id).done(function (response) {
        ALPHABET=response;
    }); 
    $.get('/report/spell?id=' + id).done(function (response) {
        SPELL=response;
    });
});


google.charts.load('current', {packages: ['bar','line']});
google.charts.setOnLoadCallback(drawChartAlphabetLetters);
google.charts.setOnLoadCallback(drawChartSpell);
function drawChartAlphabetLetters() {
    var data = new google.visualization.DataTable();
    data.addColumn('string', 'Letra');
    data.addColumn('number', 'Acertos');
    data.addColumn('number', 'Erros');
   
    data.addRows(ALPHABET);
    var colChartBefore = new google.charts.Bar(document.getElementById('colchart_before'));

    var options = {
        legend: {position: 'top'},
        height: 500,
        chart: {
            title: 'Erros e acertos de letras. Teste: Alfabeto',
            subtitle: 'Distinção dos erros e acertos do individuo no teste do alfabeto'
        }
    };
    colChartBefore.draw(data, options);
}

function drawChartSpell() {
    var data = new google.visualization.DataTable();
    data.addColumn('string', 'Palavra');
    data.addColumn('number', 'Acertos');
    data.addColumn('number', 'Erros');
    
    data.addRows(SPELL);
    var colChartBefore = new google.charts.Bar(document.getElementById('colchart_after'));

    var options = {
        legend: {position: 'top'},
        height: 500,
        chart: {
            title: 'Erros e acertos de palavras. Teste: Soletrar palavras',
            subtitle: 'Distinção dos erros e acertos do individuo no teste de soletração de palavras.'
        }
    };
    colChartBefore.draw(data, options);
}



