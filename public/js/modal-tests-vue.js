Vue.http.headers.common['X-CSRF-TOKEN'] = $('meta[name="csrf-token"]').attr('content');

var modalTests = new Vue({
    el: "#testsApp",
    data: {
        alphabetTestResults: [],
        completeTheWordTestResults: {},
        spellingTestResults: {},
        wordToImageTestResults: {},
    },
    methods: {
        getTests: function (event) {
            var id = $(event.target).data('session-id');
            var url = $("#form-get-session").attr("action");
            this.$http.post(url + "?id=" + id).then((response) => {
                this.alphabetTestResults = response.body.alphabetTestResults;
                this.completeTheWordTestResults = response.body.completeTheWordTestResults;
                this.spellingTestResults = response.body.spellingTestResults;
                this.wordToImageTestResults = response.body.wordToImageTestResults;
            }, (response) => {

            });
        }
    }
});

$(function () {
    $(".list-group-item").click(function () {
        $(this).find(".fa-gamepad").addClass("in-review");   
    });
    $("#testsModal").on('hide.bs.modal', function () {
       $(".fa-gamepad").removeClass("in-review");
    });
});



