/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function TestaCPF(strCPF) {
    var Soma;
    var Resto;

    Soma = 0;
    if (strCPF === "00000000000")
        return false;

    for (i = 1; i <= 9; i++)
        Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (11 - i);
    Resto = Soma % 11;

    if ((Resto === 0) || (Resto === 1)) {
        Resto = 0;
    } else {
        Resto = 11 - Resto;
    }
    ;

    if (Resto !== parseInt(strCPF.substring(9, 10)))
        return false;

    Soma = 0;
    for (i = 1; i <= 10; i++)
        Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (12 - i);
    Resto = Soma % 11;

    if ((Resto === 0) || (Resto === 1)) {
        Resto = 0;
    } else {
        Resto = 11 - Resto;
    }
    ;

    if (Resto !== parseInt(strCPF.substring(10, 11)))
        return false;
    return true;
}
;

function cpfInputWatcher(input) {

}

$(function () {
    $("#personal-form")
            .submit(function (e) {
                var cpfText = $("#cpf").val().replace(/\.|-/g, "");
                var formGroup = $("#cpf").parent('.form-group');
                if (!TestaCPF(cpfText)) {
                    formGroup.addClass('has-error');
                    $(".cpf-help-block").show();
                    return false;
                } else {
                    formGroup.removeClass('has-error');
                    $(".cpf-help-block").hide();
                    return true;
                }

            });
});