<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCreate()
    {
        $user = factory(App\User::class)->make();
        $response = $this->call('POST','/professional/create', $user->toArray());
        $this->assertEquals(200, $response->status());
   }
}
