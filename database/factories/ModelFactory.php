<?php

use Carbon\Carbon;

/*
  |--------------------------------------------------------------------------
  | Model Factories
  |--------------------------------------------------------------------------
  |
  | Here you may define all of your model factories. Model factories give
  | you a convenient way to create models for testing and seeding your
  | database. Just tell the factory how a default model should look.
  |
 */

$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'title' => $faker->randomElement(['Dr. ', 'Dra.', 'Prof.']),
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'cpf' => rand(10000000000, 99999999999),
        'rg' => rand(10000000000, 99999999999),
        'password' => str_random(25),
        'remember_token' => str_random(10),
    ];
});


$factory->define(App\Individual::class, function(Faker\Generator $faker) {

    $faker->addProvider(new Faker\Provider\pt_BR\Person($faker));

    $questions = new \stdClass();
    for ($i = 1; $i < 19; $i++) {
        $quest = "question$i";
        $questions->$quest = $faker->boolean();
    }

    return [
        'name' => $faker->firstName . ' ' . $faker->lastName,
        'cpf' => $faker->cpf,
        'rg' => $faker->rg,
        'sex' => $faker->randomElement(['Masculino', 'Feminino']),
        'motherName' => $faker->firstNameFemale . ' ' . $faker->lastName,
        'fatherName' => $faker->firstNameMale . ' ' . $faker->lastName,
        'telephone' => $faker->phoneNumber,
        'address' => $faker->address,
        'birthday' => $faker->dateTimeBetween('-12years', '-6years'),
        'email' => $faker->safeEmail,
        'createdBy' => App\User::where('email', 'miranda.uriel@gmail.com')->first()->_id,
        'grade' => $faker->randomElement(['1° ano', '2° ano', '3° ano', '4° ano', '5° ano']),
        'familyHistoric' => $faker->text,
        'consanguinityHistoric' => $faker->text,
        'desiredGestation' => $faker->text,
        'pregnancyComplications' => $faker->text,
        'maternalHistoric' => $faker->text,
        'neonatalComplications' => $faker->text,
        'comorbidities' => $faker->text,
        'schoolSupport' => $faker->text,
        'tdhaTreatment' => $faker->boolean(),
        'regularSchool' => $faker->boolean(),
        'speakProblem' => $faker->text,
        'individualTDHA' => $questions,
        'individualExclusion' => [
            "mentalDisability" => $faker->boolean(),
            "neurologicalDisorder" => $faker->boolean(),
            "hearingDeficiency" => $faker->boolean(),
            "visualImpairment" => $faker->boolean(),
            "nervousSystemDisease" => $faker->boolean(),
            "psychosis" => $faker->boolean(),
        ]
        
    ];
});

$factory->define(App\Session::class, function(Faker\Generator $faker) {

    $alphabet = [];


    $alphas = range('A', 'Z');


    foreach ($alphas as $i){
        $alpha = [
            'letter' => $i,
            'hit' => $faker->boolean(),
            'start' => Carbon::now(),
            'stop' => Carbon::now()->addSecond($faker->randomDigit),
            'tag' => 'alphabet'
        ];
        $alphabet[] = $alpha;
    }

    return [
        'alphabetTestResults' => $alphabet,
        'broke' => $faker->boolean(),
        'completeTheWordTestResults' => [
            'word' => $faker->randomElement(['VARINHA', 'ABELHA', 'COELHO', 'ESCOLA', 'TROFÉU', 'RAINHA']),
            'hit' => $faker->boolean(),
            'start' => Carbon::now(),
            'stop' => Carbon::now()->addSecond($faker->randomDigit),
            'tag' => 'complete-word',
        ],
        'spellingTestResults' =>
        array(
            'word' => $faker->randomElement(['VARINHA', 'ABELHA', 'COELHO', 'ESCOLA', 'TROFÉU', 'RAINHA']),
            'hit' => $faker->boolean(),
            'start' => Carbon::now(),
            'stop' => Carbon::now()->addSecond($faker->randomDigit),
            'tag' => 'spell-the-word',
        ),
        'wordToImageTestResults' =>
        array(
            'word' => $faker->randomElement(['VARINHA', 'ABELHA', 'COELHO', 'ESCOLA', 'TROFÉU', 'RAINHA']),
            'hit' => $faker->boolean(),
            'start' => Carbon::now(),
            'stop' => Carbon::now()->addSecond($faker->randomDigit),
            'tag' => 'link-word-to-image',
        ),
        'idProfessional' => \App\User::where('email','miranda.uriel@gmail.com')->first()->_id,
        'start' => Carbon::now(),
        'stop' => Carbon::now()->addMinute($faker->randomNumber(1))
    ];
});
