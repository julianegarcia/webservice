<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //$this->call(UsersTableSeeder::class);
        $this->call(TesteAppTableSeeder::class);
        factory(App\Individual::class, 100)->create()->each(function ($u) {
            $sessions = [];
            for($i=0;$i<30;$i++){
                $sessions[] = factory(App\Session::class)->make();
            }
            $u->sessions()->saveMany($sessions);
        });
        //factory(App\Session::class,30)->create();
    }

}
