<?php

use Illuminate\Database\Seeder;

use App\Test;

class TesteAppTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Test::create([
            'word' => 'COELHO',
            'tag' => 'complete-word',
            'options' => [
                [
                 'option' => 'LH',
                 'correct' => true
                ],
                [
                 'option' => 'LL',
                 'correct' => false
                ],
                [
                 'option' => 'NH',
                 'correct' => false
                ]
            ],
            'image' => 'bunny.png',
            'incompleteWord' => 'COE%sO',
        ]);
        
        Test::create([
            'word' => 'ABELHA',
            'tag' => 'complete-word',
            'options' => [
                [
                 'option' => 'LH',
                 'correct' => true
                ],
                [
                 'option' => 'LL',
                 'correct' => false
                ],
                [
                 'option' => 'NH',
                 'correct' => false
                ]
            ],
            'image' => 'bee.png',
            'incompleteWord' => 'ABE%sA',
        ]);
        
        Test::create([
            'word' => 'VARINHA',
            'tag' => 'complete-word',
            'options' => [
                [
                 'option' => 'LH',
                 'correct' => false
                ],
                [
                 'option' => 'LL',
                 'correct' => false
                ],
                [
                 'option' => 'NH',
                 'correct' => true
                ]
            ],
            'image' => 'magic_wand.png',
            'incompleteWord' => 'VARI%sA',
        ]);
        
        Test::create([
            'word' => 'ESCOLA',
            'tag' => 'complete-word',
            'options' => [
                [
                 'option' => 'LA',
                 'correct' => true
                ],
                [
                 'option' => 'LLA',
                 'correct' => true
                ],
                [
                 'option' => 'NHA',
                 'correct' => true
                ]
            ],
            'image' => 'school.png',
            'incompleteWord' => 'E%sOLA',
        ]);
        
        Test::create([
            'word' => 'TROFÉU',
            'tag' => 'complete-word',
            'options' => [
                [
                 'option' => 'TRO',
                 'correct' => true
                ],
                [
                 'option' => 'TRI',
                 'correct' => true
                ],
                [
                 'option' => 'TRE',
                 'correct' => true
                ]
            ],
            'image' => 'cup.png',
            'incompleteWord' => '%sFÉU',
        ]);
        
        Test::create([
            'word' => 'RAINHA',
            'tag' => 'complete-word',
            'options' => [
                [
                 'option' => 'NHA',
                 'correct' => true
                ],
                [
                 'option' => 'LL',
                 'correct' => true
                ],
                [
                 'option' => 'LH',
                 'correct' => true
                ]
            ],
            'image' => 'queen.png',
            'incompleteWord' => 'RAI%s',
        ]);
    }
}
